# OpenSirian

SDL Port of the Serious Sam 1 game engine released as GPL2 by Croteam. Binary compatibility with existing games is expected, but this is more of a starting point for future projects.
