add_subdirectory(Ecc)
add_subdirectory(DecodeReport)
add_subdirectory(DedicatedServer)
add_subdirectory(Depend)
add_subdirectory(Engine)
add_subdirectory(EngineGui)
add_subdirectory(EntitiesMP)
add_subdirectory(GameGUIMP)
add_subdirectory(GameMP)
#add_subdirectory(LWSkaExporter) # Lightwave SKA Exporter
add_subdirectory(MakeFONT)
#add_subdirectory(Modeler) # Requires MFC
add_subdirectory(RCon)
add_subdirectory(SeriousSam)
#add_subdirectory(SeriousSkaStudio) # Requires MFC
add_subdirectory(Shaders)
#add_subdirectory(WorldEditor) # Requires MFC
