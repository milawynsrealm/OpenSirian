/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include "MainWindow.h"
#include "resource.h"

SDL_Window *window = NULL;

void MainWindow_Init(void)
{
    // Create the main window
    window = SDL_CreateWindow("OpenSirian", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, sam_iScreenSizeI, sam_iScreenSizeJ, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        LogError(SDL_LOG_CATEGORY_ERROR, "Unable to create the window at (%dx%d).", sam_iScreenSizeI, sam_iScreenSizeJ);
        return FALSE;
    }
}

void MainWindow_End(void)
{
    // Close the window when finished
    SDL_DestroyWindow(window);
}

// close the main application window
void CloseMainWindow(void)
{
    // Close the window when finished
    SDL_DestroyWindow(window);
}

void ResetMainWindowNormal(void)
{
    SDL_HideWindow(window);
    SDL_SetWindowPosition(window, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED);
    SDL_ShowWindow(window);
}

// open the main application window for windowed mode
void OpenMainWindowNormal( PIX pixSizeI, PIX pixSizeJ)
{
    SDL_DisplayMode newMode;

    newMode.w = pixSizeI;
    newMode.h = pixSizeJ;

    /* Resize the window */
    SDL_SetWindowDisplayMode(window, &newMode);

    SDL_SetWindowFullscreen(window, 0);
    SDL_SetWindowTitle(window, "OpenSirian");
    SDL_ShowWindow(window);
}

// open the main application window for fullscreen mode
void OpenMainWindowFullScreen( PIX pixSizeI, PIX pixSizeJ)
{
    SDL_DisplayMode newMode;

    newMode.w = pixSizeI;
    newMode.h = pixSizeJ;

    /* Resize the window */
    SDL_SetWindowDisplayMode(window, &newMode);

    SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);
    SDL_SetWindowTitle(window, "OpenSirian");
    SDL_ShowWindow(window);
}

// open the main application window invisible
void OpenMainWindowInvisible(void)
{
    SDL_MinimizeWindow(window);
    SDL_SetWindowTitle(window, "OpenSirian");
}
