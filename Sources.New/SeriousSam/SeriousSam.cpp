/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <io.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <process.h>
#include <Engine/CurrentVersion.h>
#include <GameMP/Game.h>
#define DECL_DLL
#include <EntitiesMP/Global.h>
#include "resource.h"
#include "SplashScreen.h"
#include "MainWindow.h"
#include "GlSettings.h"
#include "LevelInfo.h"
#include "LCDDrawing.h"
#include "CmdLine.h"
#include "Credits.h"
#include <SDL/SDL.h>

#ifdef _WIN32
#include <windef.h>
#include <winbase.h>
#endif /* _WIN32 */

extern CGame *_pGame = NULL;

// application state variables
extern BOOL _bRunning = TRUE;
extern BOOL _bQuitScreen = TRUE;
extern BOOL bMenuActive = FALSE;
extern BOOL bMenuRendering = FALSE;

extern BOOL _bDefiningKey;
static BOOL _bReconsiderInput = FALSE;
extern PIX  _pixDesktopWidth = 0;    // desktop width when started (for some tests)

static INDEX sam_iMaxFPSActive   = 500;
static INDEX sam_iMaxFPSInactive = 10;
static INDEX sam_bPauseOnMinimize = TRUE; // auto-pause when window has been minimized
extern INDEX sam_bWideScreen = FALSE;
extern FLOAT sam_fPlayerOffset = 0.0f;

// display mode settings
extern INDEX sam_bFullScreenActive = FALSE;
extern INDEX sm_iScreenSizeI = 1024;  // current size of the window
extern INDEX sam_iScreenSizeJ = 768;  // current size of the window
extern INDEX sam_iDisplayDepth  = 0;  // 0==default, 1==16bit, 2==32bit
extern INDEX sam_iDisplayAdapter = 0;
extern INDEX sam_iGfxAPI = 0;         // 0==OpenGL, 1=Vulken?
extern INDEX sam_bFirstStarted = FALSE;
extern FLOAT sam_tmDisplayModeReport = 5.0f;
extern INDEX sam_bShowAllLevels = FALSE;
extern INDEX sam_bMentalActivated = FALSE;

// network settings
extern CTString sam_strNetworkSettings = "";
// command line
extern CTString sam_strCommandLine = "";

// 0...app started for the first time
// 1...all ok
// 2...automatic fallback
static INDEX _iDisplayModeChangeFlag = 0;
static TIME _tmDisplayModeChanged = 100.0f; // when display mode was last changed

// rendering preferences for automatic settings
extern INDEX sam_iVideoSetup = 1;  // 0==speed, 1==normal, 2==quality, 3==custom
// automatic adjustment of audio quality
extern BOOL sam_bAutoAdjustAudio = TRUE;

extern INDEX sam_bAutoPlayDemos = TRUE;
static INDEX _bInAutoPlayLoop = TRUE;

// menu calling
extern INDEX sam_bMenuSave     = FALSE;
extern INDEX sam_bMenuLoad     = FALSE;
extern INDEX sam_bMenuControls = FALSE;
extern INDEX sam_bMenuHiScore  = FALSE;
extern INDEX sam_bToggleConsole = FALSE;
extern INDEX sam_iStartCredits = FALSE;

// for mod re-loading
extern CTFileName _fnmModToLoad = CTString("");
extern CTString _strModServerJoin = CTString("");
extern CTString _strURLToVisit = CTString("");

// state variables fo addon execution
// 0 - nothing
// 1 - start (invoke console)
// 2 - console invoked, waiting for one redraw
extern INDEX _iAddonExecState = 0;
extern CTFileName _fnmAddonToExec = CTString("");

// logo textures
extern CTextureObject  _toLogoCT;
extern CTextureObject  _toLogoODI;
extern CTextureObject  _toLogoEAX;
extern CTextureObject *_ptoLogoCT  = NULL;
extern CTextureObject *_ptoLogoODI = NULL;
extern CTextureObject *_ptoLogoEAX = NULL;

extern CTString sam_strVersion = "1.10";
extern CTString sam_strModName = TRANS("-   O P E N   S O U R C E   -");

extern CTString sam_strFirstLevel = "Levels\\LevelsMP\\1_0_InTheLastEpisode.wld";
extern CTString sam_strIntroLevel = "Levels\\LevelsMP\\Intro.wld";
extern CTString sam_strGameName = "serioussamse";

extern CTString sam_strTechTestLevel = "Levels\\LevelsMP\\TechTest.wld";
extern CTString sam_strTrainingLevel = "Levels\\KarnakDemo.wld";

extern ENGINE_API INDEX snd_iFormat;

// main window canvas
CDrawPort *pdp;
CDrawPort *pdpNormal;
CDrawPort *pdpWideScreen;
CViewPort *pvpViewPort;

extern SDL_Window *window;
SDL_Event event;

static void PlayDemo(void* pArgs)
{
    CTString strDemoFilename = *NEXTARGUMENT(CTString*);
    _gmMenuGameMode = GM_DEMO;
    CTFileName fnDemo = "demos\\" + strDemoFilename + ".dem";
    extern BOOL LSLoadDemo(const CTFileName &fnm);
    LSLoadDemo(fnDemo);
}

static void ApplyRenderingPreferences(void)
{
    ApplyGLSettings(TRUE);
}

extern void ApplyVideoMode(void)
{
    StartNewMode( (GfxAPIType)sam_iGfxAPI, sam_iDisplayAdapter, sam_iScreenSizeI, sam_iScreenSizeJ,
                  (enum DisplayDepth)sam_iDisplayDepth, sam_bFullScreenActive);
}

static void BenchMark(void)
{
    _pGfx->Benchmark(pvpViewPort, pdp);
}

static void QuitGame(void)
{
  _bRunning = FALSE;
  _bQuitScreen = FALSE;
}

static FILE *_hLock = NULL;
static CTFileName _fnmLock;
static void DirectoryLockOn(void)
{
    _fnmLock = _fnmApplicationPath+"SeriousSam.loc";

    _hLock = fopen(_frmLock, "w");
    if (_hLock == NULL)
    {
        LogError("SeriousSam didn't shut down properly last time!\n");
    }
}

static void DirectoryLockOff(void)
{
    // if lock is open
    if (_hLock != NULL)
        fclose(_hLock);
}

void UpdateInputEnabledState(void)
{
    // do nothing if window is invalid
    if (window == NULL)
        return;

    // input should be enabled if application is active
    // and no menu is active and no console is active
    BOOL bShouldBeEnabled = (!IsIconic(_hwndMain) && !bMenuActive && _pGame->gm_csConsoleState==CS_OFF &&
                            (_pGame->gm_csComputerState==CS_OFF || _pGame->gm_csComputerState==CS_ONINBACKGROUND)) ||
                            _bDefiningKey;

    // if should be turned off
    if( (!bShouldBeEnabled && _bInputEnabled) || _bReconsiderInput)
    {
        // disable it and remember new state
        _pInput->DisableInput();
        _bInputEnabled = FALSE;
    }
    // if should be turned on
    if( bShouldBeEnabled && !_bInputEnabled)
    {
        // enable it and remember new state
        _pInput->EnableInput(_hwndMain);
        _bInputEnabled = TRUE;
    }
    _bReconsiderInput = FALSE;
}

void UpdatePauseState(void)
{
    BOOL bShouldPause = (_gmRunningGameMode==GM_SINGLE_PLAYER) && (bMenuActive ||
                         _pGame->gm_csConsoleState ==CS_ON || _pGame->gm_csConsoleState ==CS_TURNINGON || _pGame->gm_csConsoleState ==CS_TURNINGOFF ||
                         _pGame->gm_csComputerState==CS_ON || _pGame->gm_csComputerState==CS_TURNINGON || _pGame->gm_csComputerState==CS_TURNINGOFF);
    _pNetwork->SetLocalPause(bShouldPause);
}

void LimitFrameRate(void)
{
    // measure passed time for each loop
    static CTimerValue tvLast(-1.0f);
    CTimerValue tvNow   = _pTimer->GetHighPrecisionTimer();
    TIME tmCurrentDelta = (tvNow-tvLast).GetSeconds();

    // limit maximum frame rate
    sam_iMaxFPSActive   = ClampDn( (INDEX)sam_iMaxFPSActive,   1L);
    sam_iMaxFPSInactive = ClampDn( (INDEX)sam_iMaxFPSInactive, 1L);
    INDEX iMaxFPS = sam_iMaxFPSActive;
    if( IsIconic(_hwndMain)) iMaxFPS = sam_iMaxFPSInactive;
    if(_pGame->gm_CurrentSplitScreenCfg==CGame::SSC_DEDICATED)
        iMaxFPS = ClampDn(iMaxFPS, 60L); // never go very slow if dedicated server

    TIME tmWantedDelta = 1.0f / iMaxFPS;
    if( tmCurrentDelta<tmWantedDelta) Sleep( (tmWantedDelta-tmCurrentDelta)*1000.0f);

    // remember new time
    tvLast = _pTimer->GetHighPrecisionTimer();
}

void StartNextDemo(void)
{
    if (!sam_bAutoPlayDemos || !_bInAutoPlayLoop)
    {
        _bInAutoPlayLoop = FALSE;
        return;
    }

    // skip if no demos
    if(_lhAutoDemos.IsEmpty())
    {
        _bInAutoPlayLoop = FALSE;
        return;
    }

    // get first demo level and cycle the list
    CLevelInfo *pli = LIST_HEAD(_lhAutoDemos, CLevelInfo, li_lnNode);
    pli->li_lnNode.Remove();
    _lhAutoDemos.AddTail(pli->li_lnNode);

    // if intro
    if (pli->li_fnLevel==sam_strIntroLevel)
    {
        // start intro
        _gmRunningGameMode = GM_NONE;
        _pGame->gm_aiStartLocalPlayers[0] = 0;
        _pGame->gm_aiStartLocalPlayers[1] = -1;
        _pGame->gm_aiStartLocalPlayers[2] = -1;
        _pGame->gm_aiStartLocalPlayers[3] = -1;
        _pGame->gm_strNetworkProvider = "Local";
        _pGame->gm_StartSplitScreenCfg = CGame::SSC_PLAY1;

        _pShell->SetINDEX("gam_iStartDifficulty", CSessionProperties::GD_NORMAL);
        _pShell->SetINDEX("gam_iStartMode", CSessionProperties::GM_FLYOVER);

        CUniversalSessionProperties sp;
        _pGame->SetSinglePlayerSession(sp);

        _pGame->gm_bFirstLoading = TRUE;

        if (_pGame->NewGame( sam_strIntroLevel, sam_strIntroLevel, sp))
            _gmRunningGameMode = GM_INTRO;

        // if not intro
    }
    else
    {
        // start the demo
        _pGame->gm_StartSplitScreenCfg = CGame::SSC_OBSERVER;
        _pGame->gm_aiStartLocalPlayers[0] = -1;
        _pGame->gm_aiStartLocalPlayers[1] = -1;
        _pGame->gm_aiStartLocalPlayers[2] = -1;
        _pGame->gm_aiStartLocalPlayers[3] = -1;
        // play the demo
        _pGame->gm_strNetworkProvider = "Local";
        _gmRunningGameMode = GM_NONE;
        if( _pGame->StartDemoPlay( pli->li_fnLevel))
        {
            _gmRunningGameMode = GM_DEMO;
            CON_DiscardLastLineTimes();
        }
    }

    if (_gmRunningGameMode==GM_NONE)
        _bInAutoPlayLoop = FALSE;
}

BOOL FileExistsOnHD(const CTString &strFile)
{
    FILE *f = fopen(_fnmApplicationPath+strFile, "rb");
    if (f!=NULL)
    {
        fclose(f);
        return TRUE;
    }

    return FALSE;
}

void TrimString(char *str)
{
    int i = strlen(str);
    if (str[i-1]=='\n' || str[i-1]=='\r')
        str[i-1]=0;
}

void RunBrowser(const char *strUrl)
{
#ifdef _WIN32
    if (ShellExecuteA(GetActiveWindow(), "OPEN", strUrl, NULL, NULL, SW_SHOWMAXIMIZED) < 32)
        CPrintF("Unable to launch URL: '%s'. Make sure you have a web browser properly set.", strUrl);
#else
    CTString urlPath;

    // xdg-open is supposed to be platform independent, so it should be
    // usable on most modern *nix platforms
    urlPath = "xdg-open "+strUrl;

    if (system(urlPath) == -1)
        CPrintF("Unable to launch URL: '%s'. Make sure you have the package xdg-open installed.", strUrl);
#endif
}

void LoadAndForceTexture(CTextureObject &to, CTextureObject *&pto, const CTFileName &fnm)
{
    try
    {
        to.SetData_t(fnm);
        CTextureData *ptd = (CTextureData*)to.GetData();
        ptd->Force( TEX_CONSTANT);
        ptd = ptd->td_ptdBaseTexture;
        if( ptd!=NULL)
            ptd->Force( TEX_CONSTANT);
        pto = &to;
    } catch( char *pchrError)
    {
        (void*)pchrError;
        pto = NULL;
    }
}

void InitializeGame(void)
{
    //...
}

BOOL Init()
{
    SDL_DisplayMode displayRes;

    // Initialize the SDL Library
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
        return FALSE;

    // remember desktop width
    SDL_GetWindowDisplayMode(0, displayRes);
    _pixDesktopWidth = displayRes.w;

    MainWindow_Init();

    // parse command line before initializing engine
    ParseCommandLine(argc, argv);

    // initialize engine
    SE_InitEngine(sam_strGameName);

    //SE_LoadDefaultFonts();
    // now print the output of command line parsing
    CPrintF("%s", cmd_strOutput);

    // lock the directory
    DirectoryLockOn();

    return TRUE;
}

void End(void)
{
    MainWindow_End();

    // unlock the directory
    DirectoryLockOff();
}

// print display mode info if needed
void PrintDisplayModeInfo(void)
{
    // skip if timed out
    if( _pTimer->GetRealTimeTick() > (_tmDisplayModeChanged+sam_tmDisplayModeReport))
        return;

    // cache some general vars
    SLONG slDPWidth  = pdp->GetWidth();
    SLONG slDPHeight = pdp->GetHeight();
    if( pdp->IsDualHead()) slDPWidth/=2;

    CDisplayMode dm;
    dm.dm_pixSizeI = slDPWidth;
    dm.dm_pixSizeJ = slDPHeight;
    // determine proper text scale for statistics display
    FLOAT fTextScale = (FLOAT)slDPWidth/640.0f;

    // get resolution
    CTString strRes;
    extern CTString _strPreferencesDescription;
    strRes.PrintF( "%dx%dx%s", slDPWidth, slDPHeight, _pGfx->gl_dmCurrentDisplayMode.DepthString());
    if( dm.IsDualHead())
        strRes += TRANS(" DualMonitor");
    if( dm.IsWideScreen())
        strRes += TRANS(" WideScreen");
    if( _pGfx->gl_eCurrentAPI==GAT_OGL)
        strRes += " (OpenGL)";
#ifdef SE1_D3D
  else if( _pGfx->gl_eCurrentAPI==GAT_D3D)
      strRes += " (Direct3D)";
#endif // SE1_D3D

    CTString strDescr;
    strDescr.PrintF("\n%s (%s)\n", _strPreferencesDescription, RenderingPreferencesDescription(sam_iVideoSetup));
    strRes+=strDescr;
    // tell if application is started for the first time, or failed to set mode
    if( _iDisplayModeChangeFlag==0)
        strRes += TRANS("Display mode set by default!");
    else if( _iDisplayModeChangeFlag==2)
        strRes += TRANS("Last mode set failed!");

    // print it all
    pdp->SetFont( _pfdDisplayFont);
    pdp->SetTextScaling( fTextScale);
    pdp->SetTextAspect( 1.0f);
    pdp->PutText( strRes, slDPWidth*0.05f, slDPHeight*0.85f, LCDGetColor(C_GREEN|255, "display mode"));
}

void DoGame(void)
{
    // set flag if not in game
    if( !_pGame->gm_bGameOn)
        _gmRunningGameMode = GM_NONE;

    if( _gmRunningGameMode==GM_DEMO  && _pNetwork->IsDemoPlayFinished()
        ||_gmRunningGameMode==GM_INTRO && _pNetwork->IsGameFinished())
    {
        _pGame->StopGame();
        _gmRunningGameMode = GM_NONE;

        // load next demo
        StartNextDemo();
        if (!_bInAutoPlayLoop)
        {
            // start menu
            StartMenus();
        }
    }

    // do the main game loop
    if( _gmRunningGameMode != GM_NONE)
    {
        _pGame->GameMainLoop();
        // if game is not started
    }
    else
    {
        // just handle broadcast messages
        _pNetwork->GameInactive();
    }

    if (sam_iStartCredits>0)
    {
        Credits_On(sam_iStartCredits);
        sam_iStartCredits = 0;
    }
    if (sam_iStartCredits<0)
    {
        Credits_Off();
        sam_iStartCredits = 0;
    }
    if( _gmRunningGameMode==GM_NONE)
    {
        Credits_Off();
        sam_iStartCredits = 0;
    }

    // redraw the view
    if( !IsIconic(_hwndMain) && pdp!=NULL && pdp->Lock())
    {
        if( _gmRunningGameMode!=GM_NONE && !bMenuActive )
        {
            // handle pretouching of textures and shadowmaps
            pdp->Unlock();
            _pGame->GameRedrawView( pdp, (_pGame->gm_csConsoleState!=CS_OFF || bMenuActive)?0:GRV_SHOWEXTRAS);
            pdp->Lock();
            _pGame->ComputerRender(pdp);
            pdp->Unlock();
            CDrawPort dpScroller(pdp, TRUE);
            dpScroller.Lock();
            if (Credits_Render(&dpScroller)==0)
                Credits_Off();

            dpScroller.Unlock();
            pdp->Lock();
        }
        else
            pdp->Fill( LCDGetColor(C_dGREEN|CT_OPAQUE, "bcg fill"));

        // do menu
        if( bMenuRendering)
        {
            // clear z-buffer
            pdp->FillZBuffer( ZBUF_BACK);
            // remember if we should render menus next tick
            bMenuRendering = DoMenu(pdp);
        }

        // print display mode info if needed
        PrintDisplayModeInfo();

        // render console
        _pGame->ConsoleRender(pdp);

        // done with all
        pdp->Unlock();

        // clear upper and lower parts of screen if in wide screen mode
        if( pdp==pdpWideScreen && pdpNormal->Lock())
        {
            const PIX pixWidth  = pdpWideScreen->GetWidth();
            const PIX pixHeight = (pdpNormal->GetHeight() - pdpWideScreen->GetHeight()) /2;
            const PIX pixJOfs   = pixHeight + pdpWideScreen->GetHeight()-1;
            pdpNormal->Fill( 0, 0,       pixWidth, pixHeight, C_BLACK|CT_OPAQUE);
            pdpNormal->Fill( 0, pixJOfs, pixWidth, pixHeight, C_BLACK|CT_OPAQUE);
            pdpNormal->Unlock();
        }
        // show
        pvpViewPort->SwapBuffers();
    }
}

void TeleportPlayer(int iPosition)
{
    CTString strCommand;
    strCommand.PrintF( "cht_iGoToMarker = %d;", iPosition);
    _pShell->Execute(strCommand);
}

void RenderStarfield(CDrawPort *pdp, FLOAT fStrength)
{
    CTextureData *ptd = (CTextureData *)_toStarField.GetData();
    // skip if no texture
    if(ptd==NULL)
        return;

    PIX pixSizeI = pdp->GetWidth();
    PIX pixSizeJ = pdp->GetHeight();
    FLOAT fStretch = pixSizeI/640.0f;
    fStretch*=FLOAT(ptd->GetPixWidth())/ptd->GetWidth();

    PIXaabbox2D boxScreen(PIX2D(0,0), PIX2D(pixSizeI, pixSizeJ));
    MEXaabbox2D boxTexture(MEX2D(0, 0), MEX2D(pixSizeI/fStretch, pixSizeJ/fStretch));
    pdp->PutTexture(&_toStarField, boxScreen, boxTexture, LerpColor(C_BLACK, C_WHITE, fStrength)|CT_OPAQUE);
}

FLOAT RenderQuitScreen(CDrawPort *pdp, CViewPort *pvp)
{
    CDrawPort dpQuit(pdp, TRUE);
    CDrawPort dpWide;
    dpQuit.MakeWideScreen(&dpWide);
    // redraw the view
    if (!dpWide.Lock())
        return 0;

    dpWide.Fill(C_BLACK|CT_OPAQUE);
    RenderStarfield(&dpWide, _fLastVolume);

    FLOAT fVolume = Credits_Render(&dpWide);
    _fLastVolume = fVolume;

    dpWide.Unlock();
    pvp->SwapBuffers();

    return fVolume;
}

void QuitScreenLoop(void)
{
    Credits_On(3);
    CSoundObject soMusic;
    try
    {
        _toStarField.SetData_t(CTFILENAME("Textures\\Background\\Night01\\Stars01.tex"));
        soMusic.Play_t(CTFILENAME("Music\\Credits.mp3"), SOF_NONGAME|SOF_MUSIC|SOF_LOOP);
    } catch (char *strError)
    {
        CPrintF("%s\n", strError);
    }
    // while it is still running
    FOREVER
    {
        FLOAT fVolume = RenderQuitScreen(pdp, pvpViewPort);
        if (fVolume<=0)
            return;
        // assure we can listen to non-3d sounds
        soMusic.SetVolume(fVolume, fVolume);
        _pSound->UpdateSounds();
        // while there are any messages in the message queue
        while(SDL_PollEvent(&event) != 0)
        {
            if ((event.type == SDL_KEYDOWN) ||
                (event.type == SDL_MOUSEBUTTONDOWN))
                return;
        }
        //Sleep(5);
    }
}

void CheckBrowser(void)
{
    if (_strURLToVisit!="")
        RunBrowser(_strURLToVisit);
}

void CheckTeaser(void)
{
#if 0
    CTFileName fnmTeaser = _fnmApplicationExe.FileDir()+CTString("AfterSam.exe");
    if (fopen(fnmTeaser, "r")!=NULL)
    {
        Sleep(500);
        _execl(fnmTeaser, "\""+fnmTeaser+"\"", NULL);
    }
#endif
}

BOOL SubMain(int argc, char *argv[])
{
    if (!Init())
        return FALSE;

    // initialy, application is running and active, console and menu are off
    _bRunning = TRUE;
    _bQuitScreen = TRUE;
    _pGame->gm_csConsoleState  = CS_OFF;
    _pGame->gm_csComputerState = CS_OFF;

    while( _bRunning && _fnmModToLoad=="")
    {
        while(SDL_PollEvent(&event) != 0)
        {
            // if application should stop
            if (event.type == SDL_QUIT)
            {
                // stop running
                _bRunning = FALSE;
                _bQuitScreen = FALSE;
            }
            else if (event.type == SDL_WINDOWEVENT)
            {
                switch (event->window.event)
                {
                    // if application is deactivated
                    case SDL_WINDOWEVENT_MINIMIZED:
                    case SDL_WINDOWEVENT_FOCUS_LOST:
                    {
                        break;
                    }
                    // if application is activated
                    case SDL_WINDOWEVENT_RESTORED:
                    case SDL_WINDOWEVENT_FOCUS_GAINED:
                    {
                        break;
                    }
                }
            }
            else if (event.type == SDL_KEYDOWN)
            {
                switch( event.key.keysym.sym )
                {
                    case SDLK_ESCAPE:
                        break;
                    default:break;
                }
            }
        }
    }

    // invoke quit screen if needed
    if( _bQuitScreen && _fnmModToLoad=="") QuitScreenLoop();

    End();
    return TRUE;
}

#ifdef _WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int nCmdShow)
#else /* POSIX-based */
int main(int argc, char *argv[])
#endif
{
    int iResult = 0;

#ifdef _WIN32
    iResult = SubMain(__argc, __argv);
#else
    iResult = SubMain(argc, argv);
#endif

    CheckTeaser();

    CheckBrowser();

    // Shutdown SDL when finished
    SDL_Quit();

    return iResult;
}

// try to start a new display mode
BOOL TryToSetDisplayMode( enum GfxAPIType eGfxAPI, INDEX iAdapter, PIX pixSizeI, PIX pixSizeJ,
                          enum DisplayDepth eColorDepth, BOOL bFullScreenMode)
{
    // try to set new display mode
    BOOL bSuccess;
    if( bFullScreenMode)
    {
        bSuccess = _pGfx->SetDisplayMode( eGfxAPI, iAdapter, pixSizeI, pixSizeJ, eColorDepth);
        if( bSuccess && eGfxAPI==GAT_OGL)
            OpenMainWindowFullScreen( pixSizeI, pixSizeJ);
    }
    else
    {
        bSuccess = _pGfx->ResetDisplayMode( eGfxAPI);
        if( bSuccess && eGfxAPI==GAT_OGL)
            OpenMainWindowNormal( pixSizeI, pixSizeJ);
    }

    // if new mode was set
    if( bSuccess)
    {
        // create canvas
        ASSERT( pvpViewPort==NULL);
        ASSERT( pdpNormal==NULL);

        // if the mode is not working, or is not accelerated
        if( !bSuccess || !_pGfx->IsCurrentModeAccelerated())
        {
            CPrintF( TRANS("This mode does not support hardware acceleration.\n"));

            // destroy canvas if existing
            if( pvpViewPort!=NULL)
            {
                _pGame->DisableLoadingHook();
                _pGfx->DestroyWindowCanvas( pvpViewPort);
                pvpViewPort = NULL;
                pdpNormal = NULL;
            }

            CloseMainWindow();
            return FALSE;
        }

        // remember new settings
        sam_bFullScreenActive = bFullScreenMode;
        sam_iScreenSizeI = pixSizeI;
        sam_iScreenSizeJ = pixSizeJ;
        sam_iDisplayDepth = eColorDepth;
        sam_iDisplayAdapter = iAdapter;
        sam_iGfxAPI = eGfxAPI;

        // report success
        return TRUE;
    }
    else
    {
        CloseMainWindow();
        return FALSE;
    }

    /* Shouldn't get to here, but it has to be here to get
     * the compiler to quit complaining about it */
    return TRUE;
}

// start new display mode
void StartNewMode( enum GfxAPIType eGfxAPI, INDEX iAdapter, PIX pixSizeI, PIX pixSizeJ,
                   enum DisplayDepth eColorDepth, BOOL bFullScreenMode)
{
    CPrintF( TRANS("\n* START NEW DISPLAY MODE ...\n"));

    // try to set the mode
    BOOL bSuccess = TryToSetDisplayMode( eGfxAPI, iAdapter, pixSizeI, pixSizeJ, eColorDepth, bFullScreenMode);

    // if failed
    if( !bSuccess)
    {
        // report failure and reset to default resolution
        _iDisplayModeChangeFlag = 2;  // failure
        CPrintF( TRANS("Requested display mode could not be set!\n"));
        pixSizeI = 640;
        pixSizeJ = 480;
        bFullScreenMode = TRUE;
        // try to revert to one of recovery modes
        for( INDEX iMode=0; iMode<ctDefaultModes; iMode++)
        {
            eColorDepth = (DisplayDepth)aDefaultModes[iMode][0];
            eGfxAPI     = (GfxAPIType)  aDefaultModes[iMode][1];
            iAdapter    =               aDefaultModes[iMode][2];
            CPrintF(TRANS("\nTrying recovery mode %d...\n"), iMode);
            bSuccess = TryToSetDisplayMode( eGfxAPI, iAdapter, pixSizeI, pixSizeJ, eColorDepth, bFullScreenMode);
            if( bSuccess)
                break;
        }
        // if all failed
        if( !bSuccess)
        {
            FatalError(TRANS(
                "Cannot set display mode!\n"
                "Serious Sam was unable to find display mode with hardware acceleration.\n"
                "Make sure you install proper drivers for your video card as recommended\n"
                "in documentation and set your desktop to 16 bit (65536 colors).\n"
                "Please see ReadMe file for troubleshooting information.\n"));
        }
    }
     else // if succeeded
        _iDisplayModeChangeFlag = 1;  // all ok

    // apply 3D-acc settings
    ApplyGLSettings(FALSE);

    // remember time of mode setting
    _tmDisplayModeChanged = _pTimer->GetRealTimeTick();
}
