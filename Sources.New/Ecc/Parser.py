#!/usr/bin/python3
"""
Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""

# File handles
fInput = 0
fImplementation = 0
fDeclaration = 0
fTables = 0

strCurrentClass = ""
strLastProcName = ""
inProcHandler = 0
nextFreeId = 0
currClassId = 0
featureCanBePredictable = True

# For debugging purposes only
tmpVar = 0

def create_id():
    nextFreeId += 1
    return nextFreeId

def add_handler_function(procName, stateId):
    fDeclaration.write("  BOOL "+procName+"(const CEntityEvent &__eeInput);\n")
    fTables.write(str(hex(stateId))+", -1, CEntity::pEventHandler(&"+strCurrentClass+"::"+procName+"), ")
    fTables.write("DEBUGSTRING(\""+strCurrentClass+"::"+procName+"\")},\n")

def add_handler_function_ex(procName, stateId, baseStateId):
    fDeclaration.write("  BOOL "+procName+"(const CEntityEvent &__eeInput);\n")
    fTables.write(" {DEBUGSTRING(\""+stateId+"::"+baseStateId+"\")},\n, "+strCurrentClass+", CEntity::pEventHandler(&"+procName+"::%s),")
    inProcHandler = 0

def create_internal_handler_function(funcName, strId):
    currId = create_id()
    inProcHandler += 1
    add_handler_function("H"+hex(currId)+"_"+strLastProcName+"_"+str(inProcHandler), currId)

def declare_feature_properties():
    if featureCanBePredictable == True:
        #fTables.write(" CEntityProperty(CEntityProperty::EPT_ENTITYPTR, NULL, ("+hex(currClassId)+"<<8)+255, offsetof("+strCurrentClass", m_penPrediction), \"\", 0, 0, 0),\n")
        fImplementation.write("  m_penPrediction = NULL;\n")

class Parser:
    def __init__(self):
        tmpVar = 0
